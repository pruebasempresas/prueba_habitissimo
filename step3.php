<?php
include 'templates/header.php';
?> 
<div class="card mt-5">
	<div class="card-header text-center font-weight-bold">
		<h2>Solicita tus presupuestos en 1 minuto</h2>
	</div>
	<div class="card-header proHeader">
		<div class="progress">
		  <div class="progress-bar font-weight-bold display-5" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">3</div>
		</div>
	</div>
	<div class="card-body">
		<form action="newBudgetStep3" method="post" role="form" name="step3" id="step3" class="needs-validation" novalidate>
			<div class="form-group">
				<label for="username">Nombre</label>
				<input type="text" class="form-control" id="username" name="username" required>
				<div class="invalid-feedback iname">Introduzca su nombre</div>
			</div>
			<div class="form-group">
				<label for="email">Email</label>
				<input type="email" class="form-control" id="email" name="email" required>
				<div class="invalid-feedback">Introduzca su email</div>
				<div class="invalid-feedback ifeed d-none">No admitimos correos de hotmail.</div>
			</div> 
			<div class="form-group">
				<label for="phone">Telefono</label>
				<input type="tel" class="form-control" id="phone" name="phone" required>
				<div class="invalid-feedback itel">Introduzca su telefono</div>
			</div>
			<div class="form-group">
				<label for="address">Dirección</label>
				<input type="text" class="form-control" id="address" name="address" required>
				<div class="invalid-feedback iaddress">Introduzca su dirección</div>
			</div>
			<button type="submit" id='btnSubmit' class="btn btn-primary">Enviar</button>
		</form>
	</div>
</div>
<?php
include 'templates/footer.php';
?>