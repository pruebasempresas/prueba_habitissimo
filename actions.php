<?php
require_once 'conectionDB.php';

class Actions {
 
    private $conn;
 
    function __construct() {
        $this->conn = new ConnectionDB();
    }
 
    public function getAllBudgets()
    {
        $sql = $this->conn->actionQueries("SELECT * FROM budget");
        while ($budget = $sql->fetch_assoc()) {
            $arrayBudgets[] = $budget;
       
        }       
        return mb_convert_encoding($arrayBudgets, 'UTF-8', 'UTF-8');
    }
    public function getAllBudgetsEmail($email)
    {
        $sql = $this->conn->actionQueries("SELECT * FROM budget where email = '".$email."'");
        while ($budget = $sql->fetch_assoc()) {
            $arrayBudgets[] = $budget;
       
        }       
        return mb_convert_encoding($arrayBudgets, 'UTF-8', 'UTF-8');
    }
    public function budgetId(){
        $rs = $this->conn->actionQueries("SELECT MAX(id) AS id FROM budget");
        if ($row = $rs->fetch_row()) {
                $id = trim($row[0]);
        }
        return $id;       
    }
    public function newBudget($param){
        $sql = $this->conn->actionQueries("INSERT INTO budget (description, estimatedDate, status) values ('".$param['description']."', '".$param['estimatedDate']."', 'Pendiente')");
    }
    public function newBudgetStep2($param, $id){
        $sql = $this->conn->actionQueries("UPDATE budget SET category = '".$param['category']."', subcategory = '".$param['subcategory']."', pricePreference = '".utf8_decode($param['pricePreference'])."' WHERE id = '".$id."'");
    }  
    public function newBudgetStep3($param, $id){
        $sql = $this->conn->actionQueries("UPDATE budget SET username = '".$param['username']."', email = '".$param['email']."', phone = '".$param['phone']."', address = '".utf8_decode($param['address'])."' WHERE id = '".$id."'");
    }
    public function updateBudget($param, $id){
        $sql = $this->conn->actionQueries("UPDATE budget SET title = '".$param['title']."', description = '".$param['description']."', category = '".$param['category']."', status = '".$param['status']."' WHERE id = '".$id."'");
    }
    public function getLastBudget(){
        $sql = $this->conn->actionQueries("SELECT * FROM budget ORDER BY id DESC LIMIT 1");
        while ($budget = $sql->fetch_assoc()) {
            $arrayBudgets[] = $budget;
        }       
        return $arrayBudgets;
    }
} 
?>