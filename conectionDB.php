<?php

class ConnectionDB
{
	public $mysqli;
	private $username;
	private $password;
	private $hostname;
	private $database;

	function __construct()
	{
		$this->connectDB();		
	}

	private function connectDB()
	{	
		try{
			$username = "root";
			$password = "";
			$hostname = "localhost";
			$database = "prueba_habitissimo";

			$this->mysqli = new mysqli($hostname, $username, $password, $database);

			if($this->mysqli->connect_errno){
				printf("Connection failed: %s\n", $this->mysqli->connect_error);exit();
			}
		}catch(Exception $e){
			echo "mensaje: ".$e->message;
			exit;
		}
	}
	public function actionQueries($action)
	{
		return $this->mysqli->query($action);
	}
}