<?php
error_reporting(E_DEPRECATED & ~E_STRICT & ~E_WARNING & ~E_NOTICE);
include 'templates/header.php';
require_once 'actions.php';

$budget = new Actions();
$dataBudget = $budget->getLastBudget();

?>
<div class="card mt-5">
	<div class="card-header text-center font-weight-bold">
		<h2>Solicita tus presupuestos en 1 minuto</h2>
	</div>
	<div class="card-header proHeader">
		<div class="progress">
		  <div class="progress-bar font-weight-bold display-5" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">1</div>
		</div>
	</div>
  	<div class="card-body">
		<form action="newBudget" method="post">
			<?php foreach ($dataBudget as $row) { ?>
				<input type="hidden" name="id_budget" value="<?php echo $row['id']+1 ?>" />
			<?php } ?>
			<div class="form-group">
				<label for="description">¿Que necesitas hacer?</label>
				<textarea class="form-control" name="description" rows="3" placeholder="Introduce una breve descripcion de lo que desea hacer" required></textarea>
			</div>
			<div class="form-group">
				<label for="estimatedDate">¿Cuando quieres realizar el trabajo?</label>
				<select id="estimatedDate" class="form-control" name="estimatedDate">
					<option selected>Elije una fecha</option>
					<option>Lo antes posible</option>
					<option>de 1 a 3 meses</option>
					<option>más de 3 meses</option>
				</select>
			</div>		
			<button type="submit" class="btn btn-primary">Continuar</button>
		</form>
	</div>
</div>
<?php
include 'templates/footer.php';
?>