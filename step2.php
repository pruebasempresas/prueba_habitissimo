<?php
include 'templates/header.php';
?>
<div class="card mt-5">
	<div class="card-header text-center font-weight-bold">
		<h2>Solicita tus presupuestos en 1 minuto</h2>
	</div>
	<div class="card-header proHeader">
		<div class="progress">
		  <div class="progress-bar font-weight-bold display-5" role="progressbar" style="width: 66%;" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100">2</div>
		</div>
	</div>
	<div class="card-body">
		<form action="newBudgetStep2" method="post" name="step2" id="step2">
			<label>Categoria:</label>
			<div id="radioCategory">
			</div>
			<div class="form-group">
				<label for="subcategory">Subcategoria</label>
				<select id="subcategory" class="form-control" name="subcategory" required>
					<option value="">Elije una subcategoria</option>
				</select>
			</div>
			<div class="form-group">
				<label for="pricePreference">Preferencia precio:</label>
				<select id="pricePreference" class="form-control" name="pricePreference" required>
					<option selected>Elije una preferencia de precio</option>
					<option>Lo más barato</option>
					<option>Relación calidad precio</option>
					<option>Mejor calidad</option>
				</select>
			</div>
			<button type="submit" class="btn btn-primary">Siguiente paso</button>
		</form>
	</div>
</div>
<?php
include 'templates/footer.php';
?>