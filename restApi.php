<?php
error_reporting(E_DEPRECATED & ~E_STRICT & ~E_WARNING & ~E_NOTICE);

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
header("Access-Control-Allow-Headers: X-Requested-With");
header('Content-Type: text/html; charset=utf-8');
header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');

require_once 'actions.php';
require 'lib/Slim/Slim.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

/* Usamos GET para consultar todos los presupuestos */

$app->get('/budgets', function(){
	$response = array();
	$db = new Actions();
	$budgets = $db->getAllBudgets();
	$response["budgets"] = $budgets;
	msgResponse(200, $response);
});

/* Usamos GET para consultar todos los presupuestos */

$app->get('/budgets/:email', function($email) use ($app){
	$param['email'] = $email;
	$response = array();
	$db = new Actions();
	$budgets = $db->getAllBudgetsEmail($email);
	$response["budgets"] = $budgets;
	msgResponse(200, $response);
});
/* Usamos POST para crear un nuevo presupuesto */

$app->post('/newBudget', function() use ($app) {
	$response = array();
	$param['description'] = $app->request->post('description');
	$param['estimatedDate'] = $app->request->post('estimatedDate');
	$db = new Actions();
	$budget= $db->newBudget($param);
	if ( is_array($param) ) {
		$response["message"] = "Presupuesto creado satisfactoriamente!";
		$response["budget"] = $param;
	} else {
		$response["message"] = "Error al crear el nuevo Presupuesto.";
	}
	msgResponse(201, $response);
	$app->redirect('step2.php');
});


$app->post('/newBudgetStep2', function() use ($app) {
	$response = array();
	$param['category'] = $app->request->post('category');
	$param['subcategory'] = $app->request->post('subcategory');
	$param['pricePreference'] = $app->request->post('pricePreference');		
	$db = new Actions();
	$id = $db->budgetId();
	$budget= $db->newBudgetStep2($param, $id);
	if ( is_array($param) ) {
		$response["message"] = "Presupuesto creado satisfactoriamente!";
		$response["budget"] = $param;
	} else {
		$response["message"] = "Error al crear el nuevo Presupuesto.";
	}
	msgResponse(201, $response);
	$app->redirect('step3.php');
});

$app->post('/newBudgetStep3', function() use ($app) {
	$response = array();
	$param['username'] = $app->request->post('username');
	$param['email'] = $app->request->post('email');
	$param['phone'] = $app->request->post('phone');		
	$param['address'] = $app->request->post('address');		
	$db = new Actions();
	$id = $db->budgetId();
	$budget= $db->newBudgetStep3($param, $id);
	if ( is_array($param) ) {
		$response["message"] = "Presupuesto creado satisfactoriamente!";
		$response["budget"] = $param;
	} else {
		$response["message"] = "Error al crear el nuevo Presupuesto.";
	}
	msgResponse(201, $response);
	$app->redirect('step4.php');
});
/**


/* Usamos PUT para actualizar, por ejemplo, el ultimo presupuesto usando un formulario (presupuesto.php) */

$app->put('/updateBudget/:id', function() use ($app){
	$id = $_POST['id_budget'];
	$param['id'] = $id;
	$status = $_POST['status'];
	$param['status'] = $app->request->put('status');
	$param['title'] = $app->request->put('title');
	$param['description'] = $app->request->put('description');
	$param['category'] = $app->request->put('category');
	$db = new Actions();
	if ($param['status'] != 'Pendiente') {
		if($param['status'] == 'Publicada'){
			$response["message"] = "Error: Esta solicitud ya está publicada";
		}
		elseif($param['status'] == 'Descartada'){
			$response["message"] = "Error: Esta solicitud ya ha sido descartada";
		}else{
			$response["message"] = "Error: Esta solicitud no está en estado Pendiente";
		}
	}
	if ($param['status'] == 'Pendiente') {
		if($param['title'] != "" && $param['category'] != ""){
			$param['status'] = "Publicada";
			$budget= $db->updateBudget($param, $id);
			if ( is_array($param) ) {
				$response["message"] = "Presupuesto actualizado satisfactoriamente!";
				$response["budget"] = $param;
			} else {
				$response["message"] = "Error al actualizar el Presupuesto.";
			}
		}else{
			$param['status'] = "Descartada";
			$budget= $db->updateBudget($param, $id);
			$response["message"] = "Error: Esta solicitud está en estado Pendiente, pero no tiene categoria y/o tampoco titulo";
		} 
	}
	msgResponse(201, $response);
});

/* Ejecutamos la aplicación */
$app->run();

/**
* Mostramos la respuesta en formato json al cliente
* @param String $status_code Http response code
* @param Int $response Json response
**/
function msgResponse($status_code, $response) {
	$app = \Slim\Slim::getInstance();
	$app->status($status_code);
	$app->contentType('application/json');
	echo json_encode($response);
}

?>