$(function(){

 	'use strict';
	window.addEventListener('load', function() {
	// Fetch all the forms we want to apply custom Bootstrap validation styles to
	var forms = document.getElementsByClassName('needs-validation');
	// Loop over them and prevent submission
	var validation = Array.prototype.filter.call(forms, function(form) {
		form.addEventListener('submit', function(event) {
			if (form.checkValidity() === false) {
				event.preventDefault();
				event.stopPropagation();
			}
			form.classList.add('was-validated');
		}, false);
	});
	}, false);

	$('#username').keydown(function (e) {
		if (e.which === 9){
			$('#step3').addClass('was-validated');
			$('.invalid-feedback').hide();
			$('#phone').css('background-image', 'none');
			$('#phone').css('border-color', '#ced4da');
			$('#email').css('background-image', 'none');
			$('#email').css('border-color', '#ced4da');		
			$('#address').css('background-image', 'none');
			$('#address').css('border-color', '#ced4da');
		}
	});
	
	$('#email').keydown(function (e) {
		if (e.which === 9 || e.which === 13){
			emailVal();
		}
	});

	$('#phone').mousedown(function (e) {
		if (e.which === 1){
			$('#step3').addClass('was-validated');
			$('.invalid-feedback').hide();
			$('#phone').css('background-image', 'none');
			$('#phone').css('border-color', '#ced4da');
			$('#email').css('background-image', 'none');
			$('#email').css('border-color', '#ced4da');		
			$('#address').css('background-image', 'none');
			$('#address').css('border-color', '#ced4da');
			emailVal();
		}
	})
	$('#address').mousedown(function (e) {
		if (e.which === 1){
			$('#step3').addClass('was-validated');
			$('.invalid-feedback').hide();
			$('#phone').css('background-image', 'none');
			$('#phone').css('border-color', '#ced4da');
			$('#email').css('background-image', 'none');
			$('#email').css('border-color', '#ced4da');		
			$('#address').css('background-image', 'none');
			$('#address').css('border-color', '#ced4da');
			emailVal();
		}
	})
});

function emailVal() {
	mail = $('#email').val();
	action ='emailVal';
	$.ajax({
        data: {"email": mail, "action": action},
		url: 'handlers/handler_form.php',
		type: 'POST',
		success: function(response){                           
            if(response == 1){ //true, es decir que el email es un hotmail
               	$('#step3').addClass('was-validated');
            	$('#email').css('border-color','#dc3545');
            	$('#email').next().addClass('d-none');
            	$('.ifeed').removeClass('d-none');
            	$('.ifeed').addClass('d-block');
            	$('.itel').addClass('d-none');﻿
            	$('.iaddress').addClass('d-none');﻿
            	$('.iname').addClass('d-none');﻿
				$('#phone').attr('disabled', true);
				$('#phone').css('background-color', 'transparent');
				$('#address').attr('disabled', true);
				$('#address').css('background-color', 'transparent');
				$('#username').attr('disabled', true);
				$('#username').css('background-color', 'transparent');
				$("label[for='phone']").addClass('text-muted');
				$("label[for='phone']").addClass('font-italic');
				$("label[for='address']").addClass('text-muted');
				$("label[for='address']").addClass('font-italic');
				$("label[for='username']").addClass('text-muted');
				$("label[for='username']").addClass('font-italic');
			   	$('#btnSubmit').attr('disabled', true);
			}
           	
		},
		error: function(error, xhr){
			console.log('error');
            console.debug(xhr); 
            console.debug(error);
		}
	});
}