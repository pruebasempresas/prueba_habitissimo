$(function(){
	categories();
	$('#step2').on('click', 'input[name=category]', function() {           	
		subcategories();
	});
});
function categories(){
	const proxyurl = "https://cors-anywhere.herokuapp.com/";
	const url = "http://api.habitissimo.es/category/list";
	
	$.ajax({
		type: 'GET',
		dataType: "json",
		url: proxyurl + url,
		success: function(response){   
            for (var i = 0; i <= response.length - 1; i++) {
            	$('#radioCategory').append('<div class="form-check form-check-inline">'+
										  		'<input class="form-check-input" type="radio" name="category" value="'+response[i].id+'" required>'+
										  		'<label class="form-check-label">'+response[i].name+'</label>'+
											'</div>');
            }            
		},
		error: function(error, xhr){
			console.log('error');
            console.debug(xhr); 
            console.debug(error);
		}
	})
}
function subcategories(){
	idcategory = $('input[name=category]:checked').val();
	const proxyurl = "https://cors-anywhere.herokuapp.com/";
	const url = "http://api.habitissimo.es/category/list/"+idcategory;
	
	$.ajax({
		type: 'GET',
		dataType: "json",
		url: proxyurl + url,
		success: function(response){   
			$('select[name=subcategory]').empty();
		    $('#subcategory').append('<option value="">Elije una subcategoria</option>');
            for (var i = 0; i <= response.length - 1; i++) {
            	$('#subcategory').append('<option>'+response[i].name+'</option>');
            	}            
		},
		error: function(error, xhr){
			console.log('error');
            console.debug(xhr); 
            console.debug(error);
		}
	})
}