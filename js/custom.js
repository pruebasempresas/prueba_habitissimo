$(function(){
	status = $('#status').val();
	if(status == "Publicada" || status == "Descartada"){
		$('#title').mousedown(function (e) {
			alert('Error: No puedes modificar el titulo de un presupuesto Publicado o Descartado');
			$('#title').attr('disabled', true);
		});
		$('#description').mousedown(function (e) {
			alert('Error: No puedes modificar la descripción de un presupuesto Publicado o Descartado');
			$('#description').attr('disabled', true);
		});
		$('#category').mousedown(function (e) {
			alert('Error: No puedes modificar la categoria de un presupuesto Publicado o Descartado');
			$('#category').attr('disabled', true);
		});
	}

});