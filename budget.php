<?php
error_reporting(E_DEPRECATED & ~E_STRICT & ~E_WARNING & ~E_NOTICE);

include 'templates/header.php';
require_once 'actions.php';

$budget = new Actions();
$dateBudget = $budget->getLastBudget();

?>
<h1 class="text-center font-weight-bold">FICHA PRESUPUESTO</h1>
	<?php foreach ($dateBudget as $row) { ?>
		<form action="updateBudget/<?php echo $row['id'] ?>" method="post">
			<input type="hidden" name="id_budget" value="<?php echo $row['id'] ?>" />
			<div class="form-group">
				<label for="title">Titulo</label>
				<input type="text" class="form-control" id="title" name="title" value="<?php echo $row['title']?>">
			</div>
			<div class="form-group">
				<label for="description">Descripción</label>
				<input type="text" class="form-control" id="description" name="description" value="<?php echo $row['description']?>" required>
			</div>
			<div class="form-group">
				<label for="category">Categoria</label>
				<input type="text" class="form-control" id="category" name="category" value="<?php echo $row['category']?>">
			</div>
			<div class="form-group">
				<label for="status">Estado</label>
				<select id="status" class="form-control" name="status" required>
					<option><?php echo $row['status']?></option>
				</select>
			</div>		
			<div class="form-group">
				<label for="userdata">Usuario</label>
				<input type="text" class="form-control mb-3" id="email" name="status" value="<?php echo $row['email']?>" required disabled>
				<input type="text" class="form-control mb-3" id="address" name="status" value="<?php echo $row['address']?>" required disabled>
				<input type="text" class="form-control" id="phone" name="status" value="<?php echo $row['phone']?>" required disabled>
			</div>
	<?php } ?>
		    <input type="hidden" name="_METHOD" value="PUT"/>
			<button type="submit" class="btn btn-primary">Guardar</button>
		</form>
<?php
include 'templates/footer.php';
?>